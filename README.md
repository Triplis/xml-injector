# XML Injector

The purpose of the XML Injector mod library is to eliminate the need for many The Sims 4 mods
to include their own scripts merely to inject affordances to the various affordance lists of
game objects.  Performing these injections via scripting is frequently necessary to eliminate
the possibility of conflicts between mods that want to modify the same game objects,
particularly the object_sim tuning.

Using the XML Injector eliminates any need for the modder to understand the methods of writing
scripts to modify XML tuning at game startup, compiling and maintaining these additional
scripts which only serve to accomplish a simple task.  Instead, the modder simply creates
an easy to write XML snippet and includes this in their mod package.