# XML Injector v1
# by Scumbumbo @ MTS
#
# The snippet module defines the tuning classes to load the XmlInjector snippet XML.  Once
# the tuning has been loaded by the game, the _tuning_loaded_callback invokes the functions
# in the add_to_tuning module to process the affordance additions.
#
# This mod is intended as a standard for modder's to use as a shared library.  Please do not
# distribute any modifications anywhere other than the mod's main download site.  Modification
# suggestions and bug notices should be communicated to the maintainer, currently Scumbumbo at
# the Mod The Sims website - http://modthesims.info/member.php?u=7401825
#
import services
import sims4.log
from sims4.resources import Types
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import AutoFactoryInit, HasTunableReference, HasTunableSingletonFactory, Tunable, TunableTuple, TunableList, TunableReference, TunableVariant
from objects.definition_manager import DefinitionManager
import xml_injector.add_to_tuning

import traceback
    
logger = sims4.log.Logger('XmlInjector')

class ObjectSelection(TunableVariant):
    # object_list variant
    class _ObjectList(HasTunableSingletonFactory, AutoFactoryInit):
        FACTORY_TUNABLES = {
            'object_list': TunableList(
                description = 'A list of objects to add the interactions to',
                tunable = Tunable(
                    description = 'Reference to an object tuning instance',
                    tunable_type = int,
                    default = None)
                )
            }
        def get_objects(self):
            # Get the object tunings for each of the objects in the object list
            # from the DefinitionManager
            definition_manager = services.definition_manager()
            obj_list = []
            for obj_id in self.object_list:
                # get() on the DefinitionManager will return an object definition,
                # to get an actual tuning by ID, we need to call the super()
                tun = super(DefinitionManager, definition_manager).get(obj_id)
                if tun:
                    if hasattr(tun, '_super_affordances'):
                        obj_list.append(tun)
            return obj_list

    # objects_with_affordance variant
    class _ObjectsWithAffordance(HasTunableSingletonFactory, AutoFactoryInit):
        FACTORY_TUNABLES = {
            'affordance': TunableReference(
                description = 'Reference to an interaction tuning instance',
                manager = services.affordance_manager(),
                class_restrictions=('SuperInteraction',),
                allow_none = False,
                pack_safe = True)
            }
        def get_objects(self):
            # Iterate through all object tunings from the DefinitionManager
            # and return those that contain the referenced affordance
            definition_manager = services.definition_manager()
            obj_list = []
            for tun in definition_manager._tuned_classes.values():
                if hasattr(tun, '_super_affordances') and self.affordance in tun._super_affordances:
                    obj_list.append(tun)
            return obj_list

    # objects_matching_name variant
    class _ObjectsMatchingName(HasTunableSingletonFactory, AutoFactoryInit):
        FACTORY_TUNABLES = {
            'partial_name': Tunable(
                description = 'A string specifying the partial name of objects to select',
                    tunable_type = str,
                    default = None)
            }
        def get_objects(self):
            # Iterate through all object tunings from the DefinitionManager
            # and return those whose name contains the partial_name
            obj_list = []
            if not isinstance(self.partial_name, str):
                logger.error('Tuning error, missing or invalid partial_name')
            else:
                definition_manager = services.definition_manager()
                for tun in definition_manager._tuned_classes.values():
                    if hasattr(tun, '__name__') and self.partial_name in tun.__name__:
                        obj_list.append(tun)
            return obj_list

    # Create a variant for the object_selection
    def __init__(self, **kwargs):
        super().__init__(
            object_list = ObjectSelection._ObjectList.TunableFactory(),
            objects_with_affordance = ObjectSelection._ObjectsWithAffordance.TunableFactory(),
            objects_matching_name = ObjectSelection._ObjectsMatchingName.TunableFactory(),
            default = None,
            **kwargs)

# XmlInjector snippet tuning class
class XmlInjector(HasTunableReference, metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(Types.SNIPPET)):
    INSTANCE_TUNABLES = {
        'add_interactions_to_objects': TunableList(
            description = 'A list of object and interaction lists',
            tunable = TunableTuple(
                object_selection = ObjectSelection(),                    
                _super_affordances = TunableList(
                    description = 'A list of interactions to add to the objects',
                    tunable = TunableReference(
                        description = 'Reference to an interaction tuning instance',
                        manager = services.affordance_manager(),
                        class_restrictions=('SuperInteraction',),
                        allow_none = False,
                        pack_safe = True)
                    )
            ),
            allow_none = False,
            unique_entries = True
        ),
        'add_interactions_to_sims': TunableList(
            description = 'A list of interactions to add to the object_sim',
            tunable = TunableReference(
                        description = 'Reference to an interaction tuning instance',
                        manager = services.affordance_manager(),
                        class_restrictions=('SuperInteraction',),
                        allow_none = False,
                        pack_safe = True),
            allow_none = False,
            unique_entries = True
        ),
        'add_interactions_to_phones': TunableList(
            description = 'A list of interactions to add to sim phones',
            tunable = TunableReference(
                        description = 'Reference to an interaction tuning instance',
                        manager = services.affordance_manager(),
                        allow_none = False,
                        pack_safe = True),
            allow_none = False,
            unique_entries = True
        ),
        'add_interactions_to_relationship_panel': TunableList(
            description = 'A list of interactions to add to the relationship panel',
            tunable = TunableReference(
                        description = 'Reference to an interaction tuning instance',
                        manager = services.affordance_manager(),
                        allow_none = False,
                        pack_safe = True),
            allow_none = False,
            unique_entries = True
        ),
        'add_mixer_interactions': TunableList(
            description = 'A list of mixer_snippet and interaction lists',
            tunable = TunableTuple(
                mixer_snippets = TunableList(
                    description = 'A list of AffordanceLists to add the interactions to',
                    tunable = TunableReference(
                        description = 'Reference to an AffordanceList snippet tuning instance',
                        manager = services.get_instance_manager(Types.SNIPPET),
                        class_restrictions=('AffordanceList',),
                        allow_none = False,
                        pack_safe = True)
                    ),
                affordances = TunableList(
                    description = 'A list of interactions to add to the mixers',
                    tunable = TunableReference(
                        description = 'Reference to an interaction tuning instance',
                        manager = services.affordance_manager(),
                        allow_none = False,
                        pack_safe = True)
                    )
            ),
            allow_none = False,
            unique_entries = True
        )
    }

    @classmethod
    def _tuning_loaded_callback(cls):
        logger.info('Processing {}', str(cls))
        try:
            for entry in cls.add_interactions_to_objects:
                if isinstance(entry['object_selection'],str) or entry['object_selection'] is None:
                    logger.error('Tuning error, missing or invalid object_selection')
                else:
                    xml_injector.add_to_tuning.add_super_affordances_to_objects(entry['object_selection'], entry['_super_affordances'])
            if cls.add_interactions_to_sims:
                xml_injector.add_to_tuning.add_super_affordances_to_sims(cls.add_interactions_to_sims)
            if cls.add_interactions_to_phones:
                xml_injector.add_to_tuning.add_super_affordances_to_phones(cls.add_interactions_to_phones)
            if cls.add_interactions_to_relationship_panel:
                xml_injector.add_to_tuning.add_super_affordances_to_relpanel(cls.add_interactions_to_relationship_panel)
            for entry in cls.add_mixer_interactions:
                xml_injector.add_to_tuning.add_mixer_to_affordance_list(entry['mixer_snippets'], entry['affordances'])
        except:
            logger.error('Exception occurred processing XmlInjector tuning instance {}', str(cls))
            logger.error(traceback.format_exc())

    def __repr__(self):
        return '<XmlInjector:({})>'.format(self.__name__)
    def __str__(self):
        return '{}'.format(self.__name__)
