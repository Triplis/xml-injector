# XML Injector v1
# by Scumbumbo @ MTS
#
# Functions in the add_to_tuning module are called by the snippet's _tuning_loaded_callback
# in order to process the addition of affordances to the various game objects.
#
# This mod is intended as a standard for modder's to use as a shared library.  Please do not
# distribute any modifications anywhere other than the mod's main download site.  Modification
# suggestions and bug notices should be communicated to the maintainer, currently Scumbumbo at
# the Mod The Sims website - http://modthesims.info/member.php?u=7401825
#

import services
import sims4.log
from objects.definition_manager import DefinitionManager

logger = sims4.log.Logger('XmlInjector')

OBJECT_SIM = 14965  # The instance ID for the object_sim tuning
TESTING = False     # If testing then allow adding multiple copies of affordance to _super_affordances

def add_super_affordances_to_objects(object_selection, sa_list):
    for tun in object_selection.get_objects():
        if hasattr(tun, '_super_affordances'):
            sa_to_add_list = []
            for sa in sa_list:
                if not sa in tun._super_affordances or TESTING:
                    sa_to_add_list.append(sa)
            if len(sa_to_add_list) > 0:
                logger.info('{}: adding super_affordances: {}', tun, sa_to_add_list)
                tun._super_affordances += tuple(sa_to_add_list)

def add_super_affordances_to_sims(sa_list):
    definition_manager = services.definition_manager()
    object_sim = super(DefinitionManager, definition_manager).get(OBJECT_SIM)
    sa_to_add_list = []
    for sa in sa_list:
        if not sa in object_sim._super_affordances or TESTING:
            sa_to_add_list.append(sa)
    if len(sa_to_add_list) > 0:
        logger.info('{}: adding super_affordances: {}', object_sim, sa_to_add_list)
        object_sim._super_affordances += tuple(sa_to_add_list)

def add_super_affordances_to_phones(sa_list):
    definition_manager = services.definition_manager()
    object_sim = super(DefinitionManager, definition_manager).get(OBJECT_SIM)
    sa_to_add_list = []
    for sa in sa_list:
        if not sa in object_sim._phone_affordances or TESTING:
            sa_to_add_list.append(sa)
    if len(sa_to_add_list) > 0:
        logger.info('phones: adding super_affordances: {}', sa_to_add_list)
        object_sim._phone_affordances += tuple(sa_to_add_list)

def add_super_affordances_to_relpanel(sa_list):
    definition_manager = services.definition_manager()
    object_sim = super(DefinitionManager, definition_manager).get(OBJECT_SIM)
    sa_to_add_list = []
    for sa in sa_list:
        if not sa in object_sim._relation_panel_affordances or TESTING:
            sa_to_add_list.append(sa)
    if len(sa_to_add_list) > 0:
        logger.info('relpanel: adding super_affordances: {}', sa_to_add_list)
        object_sim._relation_panel_affordances += tuple(sa_to_add_list)

def add_mixer_to_affordance_list(affordance_lists_list, mixer_list):
    for affordance_list in affordance_lists_list:
        mixers_to_add_list = []
        for mixer in mixer_list:
            if not mixer in affordance_list.value or TESTING:
                mixers_to_add_list.append(mixer)
        if len(mixers_to_add_list) > 0:
            logger.info('{}: adding mixer interactions: {}', affordance_list, mixers_to_add_list)
            affordance_list.value += tuple(mixers_to_add_list)
